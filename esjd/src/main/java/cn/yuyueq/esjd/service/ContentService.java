package cn.yuyueq.esjd.service;


import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *
 * </p>
 *
 * @author wenxin.du
 * @since 2021/9/22
 */
public interface ContentService {

    Boolean parseContent(String keyword) throws IOException;

    List<Map<String, Object>> searchPage(String keyword, int pageNo, int pageSize) throws IOException;
}

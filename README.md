# esjd

---

### 介绍
 **ES-仿京东搜索-爬虫** 

--- 
:point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/113653_6e20dcfe_7691372.png "屏幕截图.png")

--- 
:point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/113828_ba7794f6_7691372.png "屏幕截图.png")

--- 
:point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: :point_down: 


![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/113930_cfc01851_7691372.png "屏幕截图.png")

---

#### 使用说明

1.  es-api中是一些简单的关于Elasticsearch的增删改查操作，可以简单入门
2.  esjd中是最后可以使用的一个搜索demo（建议是看一下视频，以及学一下es-api中的简单操作）
3.  参考于B站狂神说

---
#### 软件架构

```xml
 <properties>
        <java.version>1.8</java.version>
        <elasticsearch.version>7.14.1</elasticsearch.version>
        <repackage.classifier/>
        <spring-native.version>0.10.3</spring-native.version>
    </properties>
    <dependencies>

        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.76</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-elasticsearch</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.experimental</groupId>
            <artifactId>spring-native</artifactId>
            <version>${spring-native.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

```



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


